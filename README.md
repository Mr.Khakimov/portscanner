Simple Python3 port scanner

Build the Executable:
Open a terminal or command prompt, navigate to the directory containing PortScan.py, and run the following command:

>pip install

>pyinstaller --onefile main.py

This will create a standalone executable file named PortScan (or PortScan.exe on Windows) in the dist directory.

Run the Executable:
You can run the generated executable from the terminal or command prompt. For example:

>./dist/PortScan IP_ADDRESS START_PORT END_PORT
Replace IP_ADDRESS, START_PORT, and END_PORT with the appropriate values.

That's it! You've created a simple port scanner script and built it into an executable file using PyInstaller.