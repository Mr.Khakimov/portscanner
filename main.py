import socket


GREEN = '\033[92m'
RED = '\033[91m'
BLUE = '\033[94m'
YELLOW = '\033[93m'
CYAN = '\033[96m'
MAGENTA = '\033[95m'
RESET = '\033[0m'  # Reset to default color

# Example usage colors
# print(GREEN + '[+] Success Message' + RESET)
# print(RED + '[-] Error Message' + RESET)
# print(BLUE + '[+] Information Message' + RESET)
# print(YELLOW + '[+] Warning Message' + RESET)
# print(CYAN + '[+] Special Message' + RESET)
# print(MAGENTA + '[+] Another Special Message' + RESET)

def udpScan(ip, startPort, endPort):
    print(BLUE + '[*] Starting UDP port scan on host %s \n' % ip + RESET)
    for port in range(startPort, endPort + 1):
        try:
            udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            udp.settimeout(0.1)  # Adjust timeout as needed
            udp.sendto(b'', (ip, port))
            data, addr = udp.recvfrom(1024)
            print(YELLOW + '[+] %s:%d/UDP Open' % (ip, port))
        except socket.timeout:
            pass
            # for show closed ports
            #print('[-] %s:%d/UDP Closed' % (ip, port))
        # except Exception as e:
        #     print('[-] Error: %s' % e)
        finally:
            udp.close()
    print(GREEN + '\n[+] UDP scan on host %s complete' % ip + RESET)

def tcpScan(ip, startPort, endPort):
    print(BLUE + '[*] Starting TCP port scan on host %s \n' % ip + RESET)
    for port in range(startPort, endPort + 1):
        try:
            tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if not tcp.connect_ex((ip, port)):
                print(YELLOW + '[+] %s:%d/TCP Open' % (ip, port))
                tcp.close()
        except Exception:
            pass
    print(GREEN + '\n[+] TCP scan on host %s complete' % ip + RESET)

def main():
    socket.setdefaulttimeout(0.01)
    network = input("IP ADDRESS: ")
    startPort = int(input("START PORT: "))
    endPort = int(input("END PORT: "))

    scan_type = input("Enter scan type (TCP or UDP): ").upper()

    if scan_type == "TCP":
        tcpScan(network, startPort, endPort)
    elif scan_type == "UDP":
        udpScan(network, startPort, endPort)
    else:
        print("Invalid scan type. Please choose TCP or UDP.")

if __name__ == '__main__':
    main()
    end = input("Press any key to close")